# tts-scripts

Text-to-speech scripts that make our experience living in Te Whanganui-a-tara a bit more bearable. Text from
various websites is downloaded, then translated into german.

* wgtnrubbish.sh - what should we put out with the rubbish collection today?
* wgtnweather.sh - what is the weather looking like today?

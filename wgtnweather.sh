#!/bin/sh

if [ ! -e ~/.weather.lock ] ;
  then touch ~/.weather.lock
  export XDG_RUNTIME_DIR=/run/user/1001
  ~/metservice_biking.py > ~/.weatherOutput.txt
  speakText="$(cat ~/.weatherOutput.txt)"
  speakTrans=$(cat ~/.weatherOutput.txt | \
     perl -pe 's/^Summary: Cloudy/wolkig/im' | \
     perl -pe 's/^Summary: Drizzle/Nieselregen/im' | \
     perl -pe 's/^Summary: Few showers/leichte Regenschauer/im' | \
     perl -pe 's/^Summary: Fine/gut/im' | \
     perl -pe 's/^Summary: Fog/neblig/im' | \
     perl -pe 's/^Summary: Hail/Hagel/im' | \
     perl -pe 's/^Summary: Partly cloudy/leicht bewölkt/im' | \
     perl -pe 's/^Summary: Rain/Regen/im' | \
     perl -pe 's/^Summary: Showers/Schauer/im' | \
     perl -pe 's/^Summary: Snow/Schnee/im' | \
     perl -pe 's/^Summary: Thunder/Donner/im' | \
     perl -pe 's/^Summary: Windy/windig/im' | \
     perl -pe 's/^Summary: Frost/frostig/im');
  echo "${speakTrans}";
  espeak -v de "${speakTrans}";
  echo $(whoami) >> ~/.weather.log
  env >> ~/.weather.log
  echo "${speakText} / ${speakTrans}" >> ~/.weather.log
  rm ~/.weather.lock
fi
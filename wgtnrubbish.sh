#!/bin/sh

if [ ! -e ~/.rubbish.lock ] ;
  then touch ~/.rubbish.lock
  export XDG_RUNTIME_DIR=/run/user/1001
  # Id 6830 is for Karori [but not where we live]
  speakText="$(curl -L -s 'http://tinyurl.com/wgtnrubbish?streetId=6830&addWeeks=0' | grep 'recycling-icon' | perl -pe 's/<.*?>//g;s/\s+/ /g')"
  espeak -v de 'Diese woche ist...';
  echo ${speakText};
  speakTrans=$(echo ${speakText} | \
     perl -pe 's/Rubbish Wheelie bin or recycling bags/Müll, Wiederverwertungstonne, oder, Wiederverwertungstüten/i' | \
     perl -pe 's/Rubbish Glass crate/Müll, Glaskiste/i');
  espeak -v de "${speakTrans}";
  echo $(whoami) >> ~/.rubbish.log
  env >> ~/.rubbish.log
  echo "${speakText} / ${speakTrans}" >> ~/.rubbish.log
  rm ~/.rubbish.lock
fi

#!/usr/bin/env python3

## based off https://github.com/arfar/metservice-api-py

import urllib.request
from urllib.error import HTTPError
import sys
import json
import pprint

METSERVICE_BASE = 'http://metservice.com/publicData/'
# It's a little bit ugly, but it allows me to iterate over them later
API_OPTIONS = {
    'LOCAL_FORECAST': 'localForecast',
    'HOURLY_OBS_AND_FORECAST': 'hourlyObsAndForecast_',
}

WIND_LOOKUP = {
  'N': 'Nord', 'NE': 'Nordost', 'E': 'Ost', 'SE': 'Sudost',
  'S': 'Sud', 'SW': 'Sudwest', 'W': 'West', 'NW': 'Nordwest',
}

def get_response(url):
    try:
        response = urllib.request.urlopen(url)
    except HTTPError:
        return None
    return json.loads(response.read())

if __name__ == '__main__':
    try:
        city = sys.argv[1]
    except IndexError:
        city = 'wellington'

    for key in API_OPTIONS.keys():
        url = ''.join([METSERVICE_BASE, API_OPTIONS[key], city])
        res = get_response(url)
        if(key == 'LOCAL_FORECAST'):
            print("Das Wetter für heute ist...")
            day0 = res["days"][0]
            print("Summary: " + day0["forecastWord"] + ".")
            print("{} bis {} grad.".format(day0["min"], day0["max"]))
        elif(key == "HOURLY_OBS_AND_FORECAST"):
            firstDate = res["actualData"][0]["date"]
            gustSpeedMin = 200
            gustSpeedMax = 0
            rainAmount = 0
            windDirs = dict()
            maxWindDirCount = 0
            maxWindDir = ""
            allData = res["actualData"] + res["forecastData"]
            for hourlyObs in allData:
                if(hourlyObs["date"] == firstDate):
                    if(float(hourlyObs["gustSpeed"]) > gustSpeedMax):
                        gustSpeedMax = float(hourlyObs["gustSpeed"])
                    if(float(hourlyObs["gustSpeed"]) < gustSpeedMin):
                        gustSpeedMin = float(hourlyObs["gustSpeed"])
                    rainAmount += float(hourlyObs["rainFall"])
                    if(hourlyObs["windDir"] in windDirs):
                        windDirs[hourlyObs["windDir"]] += 1
                    else:
                        windDirs[hourlyObs["windDir"]] = 1
            print(f"Wind {gustSpeedMin:.0f} - {gustSpeedMax:.0f} kilometer pro Stunde.")
            if(rainAmount == 0):
                print("Kein Regen.")
            else:
                print(f"{rainAmount:.1f} millimeter Regen.")
            for windDir in windDirs.keys():
                if(windDirs[windDir] > maxWindDirCount):
                     maxWindDirCount = windDirs[windDir]
                     maxWindDir = WIND_LOOKUP[windDir]
                elif(windDirs[windDir] == maxWindDirCount):
                     maxWindDir = maxWindDir + ", " + WIND_LOOKUP[windDir]
            if(maxWindDirCount <= 2):
                print("Wind von überall.")
            else:
                print(f"Wind von {maxWindDir}.")